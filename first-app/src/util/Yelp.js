const apiKey = "Ek5h37aMxi1mb2m5etmipkWiB6zB8PjnZ5xsUmjr8O8jtgvAASz_-It9buNNzqY3PTjBEmvKA9lCIX_5afGoXdsSZXb5WdH2jt-SUlsJR331yK2eFuLPhFPPOI8GXHYx";
const Yelp = {
    search(term, location, sortBy) {
        return fetch(`https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/search?term=${term}&location=${location}&sort_by=${sortBy}`, {
            headers: {
                Authorization: `Bearer ${apiKey}`
            }
        } ).then((response) => {
            return response.json();
        }).then((jsonResponse) => {
            if(jsonResponse.business){
                return jsonResponse.businesses.map(business => {
                    return {
                            id: business.id,
                            imageSrc: business.image_url,
                            // more key/values from the response
                            name: business.name,
                            address: business.location.address1,
                            city: business.location.city,
                            state: business.location.state,
                            zipCode: business.location.zip_code,
                            category: business.category,
                            rating: business.rating,
                            reviewCount: business.review_count
                    }
                });
            }
        })
    }
}

export default Yelp;